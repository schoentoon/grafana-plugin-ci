FROM golang:1.19 AS golang

FROM node:lts

COPY --from=golang /usr/local/go/ /usr/local/go/

# the final step of ci (./node_modules/.bin/grafana-toolkit plugin:ci-package) seems to need this?
RUN npm install rimraf -g

ENV PATH /usr/local/go/bin:/root/go/bin:$PATH

# we need this to build golang based datasources
RUN go install github.com/magefile/mage@latest

RUN wget -O golangci-lint.deb https://github.com/golangci/golangci-lint/releases/download/v1.48.0/golangci-lint-1.48.0-linux-amd64.deb && \
    dpkg -i golangci-lint.deb && \
    rm golangci-lint.deb

RUN apt-get update && apt-get install --no-install-recommends -y zip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
